# -*- coding: utf-8 -*-


import networkx as nx
import random
import time
 
# Moje rozwiazanie
def mySolution_path(graph, startNode, targetNode):
    # Skopiowanie obiektu
    graph = graph.copy()
 
    # Ustawiamy aktualny wierzchołek na taki jaki podano na początku
    currentNode = startNode
 
    # Słownik z "Kosztem dotarcia"
    d = {}
 
    # Słownik z "Wierzchołek docelowy": "Wierzchołek z którego idziemy"
    p = {}
 
    # Uzupełnienie słowników
    for key in graph.nodes():
        d[key] = -1
        p[key] = -1
 
    d[startNode] = 0
 
    # Algorytm wyszukiwania drogi
    while len(graph.nodes()) > 0:
 
        # Resetujemy aktualny wierzchołek
        currentNode = -1
 
        # Wyszukujemy wierzchołek o aktualnie najmniejszym koszcie
        for key in graph.nodes():
            if d[key] != -1 and (currentNode == -1 or d[currentNode] > d[key]):
                currentNode = key
 
        # Jeżeli nie odnaleźliśmy wierzchołka to koniec pracy...
        if currentNode == -1:
            return False
 
        # Sprawdzamy wszystkie drogi z aktualnego wierzchołka do najbliższych
        for nodeTarget in graph.neighbors(currentNode):
 
            node = graph.get_edge_data(currentNode, nodeTarget)
            tempTargetNode = nodeTarget
            targetValue = d[tempTargetNode]
 
            # Jeżeli aktualny koszt jest nie znany bądź zapisany jest większy od aktualnego kosztu
            if targetValue == -1 or targetValue > d[currentNode] + node["weight"]:
                d[tempTargetNode] = d[currentNode] + node["weight"]
                p[tempTargetNode] = currentNode
 
        # Usunięcie aktualnie przetwarzanego wierzchołka z podanej przez klienta listy
        #nodes.pop(currentNode, None)
        graph.remove_node(currentNode)
 
    #print 'P: ', p
    #print 'D: ', d
 
    result = []
    currentNode = targetNode
    while True:
        result.append(currentNode)
 
        if currentNode == startNode:
            break
 
        currentNode = p[currentNode]
 
    # Odwracamy wyniki
    result.reverse()
 
    # Zwracamy wyniki
    return result
 
 
 
# Generowanie grafów
def generateGraphes():
    # Tablica przechowujaca grafy dostępna tylko wewnątrz funkcji
    graphes = []
 
    for i in range(0, 100):
        nodes = random.randint(10, 101)
 
        # Tworzymy losowy graf od 10 do 100 wierzcholkow
        randomGraph = nx.gnp_random_graph(nodes, 100, 10, True)
 
        # Generujemy losowe wagi
        for u, v, d in randomGraph.edges(data=True):
            # Losujemy wagę
            d['weight'] = (int) (random.random() * 1000)
 
        graphes.append(randomGraph)
 
    return graphes
 
# Kod testów networkX
def testNetworkX(graph):
    startTime = time.time()
 
    # Sprawdzamy czy istnieje sciezka docelowa aby uniknac wyjatkow..
    if nx.has_path(graph, 0, 3):
        # Wyszukujemy sciezke
        resultList = nx.dijkstra_path(graph, 0, 3)
    else:
        resultList = False
 
    finishTime = time.time()
 
    return {
        'list': resultList,
        'time': finishTime - startTime
    }
 
def testmySolution(graph):
    startTime = time.time()
 
    # My nie rzucamy wyjatkiem
    resultList = mySolution_path(graph, 0, 3)
 
    finishTime = time.time()
 
    return {
        'list': resultList,
        'time': finishTime - startTime
    }
 
 
# Tablica przechowujaca grafy
graphes = generateGraphes()
 
print ('Testy przeprowadzam dla ', len(graphes), ' grafow')
 
# Lista czasow
timers = {
    'network': 0,
    'mySolution': 0
}
 
# Dla każdego wygenerowanego grafu...
for graph in graphes:
    networkX = testNetworkX(graph)
    mySolution = testmySolution(graph)
 
    if not (networkX['list'] == mySolution['list']):
        print ('Rozne rozwiazania !')
 
    timers['network'] = timers['network'] + networkX['time']
    timers['mySolution'] = timers['mySolution'] + mySolution['time']
 
print ('Czas wykonywania dla NetworkX', timers['network'])
print ('Czas wykonywania dla mySolution', timers['mySolution'])

